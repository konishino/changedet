#include <cmath>
#include <iostream>
#include <Python.h>

#include "numpy_helper.hpp"
using namespace std;

void _block_match(const npyArray<float>& left, const npyArray<float>& right,
                npyArray<float> mad_out, const int win_size, const int max_disp) {

    const int height = left.size(0);
    const int width = left.size(1);
    const int r = win_size/2;

#pragma omp parallel for collapse(3)
    for(int y=0; y < height; ++y) {
    for(int x=0; x < width; ++x) {
    for(int o=-max_disp; o<=max_disp; ++o) {
        int npix = 0;
        float sad = 0;

        for(int i=-r; i<=r; ++i) {
        for(int j=-r; j<=r; ++j) {
            const int yi = y+i;
            const int xi_L = x+j;
            const int xi_R = x+j+o;
            if(yi >= 0 && xi_L >= 0 && xi_R >= 0 && yi < height && xi_L < width && xi_R < width) {
                sad += abs( left(yi, xi_L) - right(yi, xi_R) ); 
                ++npix;
            }
        }}

        if (npix > 0) {
            mad_out(y, x, o+max_disp) = sad / npix;
        } else {
            mad_out(y, x, o+max_disp) = -1;
        }
    }}}
}

/**
 * PYTHON METHOD DEFS
 */
static PyObject* block_match(PyObject* self, PyObject* args) {
    PyArrayObject* left_ptr;
    PyArrayObject* right_ptr;
    int win_size, max_disp;

    if(!PyArg_ParseTuple(args, "O!O!ii",
                &PyArray_Type, &left_ptr,
                &PyArray_Type, &right_ptr,
                &win_size, &max_disp)){
        return NULL;
    }

    if(PyArray_TYPE(left_ptr) != NPY_FLOAT32 || PyArray_TYPE(right_ptr) != NPY_FLOAT32) {
        PyErr_SetString(PyExc_TypeError, "Inputs must have be of type float32.");
        return nullptr;
    }
    if(PyArray_NDIM(left_ptr) != 2 || PyArray_NDIM(right_ptr) != 2) {
        PyErr_SetString(PyExc_TypeError, "Inputs must be 2D arrays.");
        return nullptr;
    }

    const npyArray<float> left(left_ptr);
    const npyArray<float> right(right_ptr);

    const int height = left.size(0);
    const int width = left.size(1);
    npy_intp dims[] = {height, width, 2*max_disp+1};
    PyArrayObject* mad_ptr = (PyArrayObject*)PyArray_ZEROS(3, dims, NPY_FLOAT, 0);
    npyArray<float> mad(mad_ptr);

    _block_match(left, right, mad, win_size, max_disp);
    return PyArray_Return(mad_ptr);
}

static PyMethodDef matchMethods[] = {
    {"block_match", block_match, METH_VARARGS, "Compute block matching scores for rectified stereo pairs."},
    {NULL, NULL, 0, NULL}
};

extern "C" {
#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef match_moduledef =
{
    PyModuleDef_HEAD_INIT,
    "match",
    "Stereo block matching.",
    -1,     /* size of per-interpreter state of the module,
               or -1 if the module keeps state in global variables. */
    matchMethods
};

PyMODINIT_FUNC PyInit_match(void)
#else
PyMODINIT_FUNC initmatch(void)
#endif
{
    PyObject* m;
#if PY_MAJOR_VERSION == 3
    m = PyModule_Create(&match_moduledef);
#else
    m = Py_InitModule3("match", module_methods, "Stereo Block Matching");
#endif

    import_array();

#if PY_MAJOR_VERSION >= 3
    return m;
#endif
}
}
