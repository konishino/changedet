import sys

import cv2
import numpy as np

def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res

"""
Load and display inputs
"""
img1, img2 = [cv2.imread(f) for f in sys.argv[1:3]]
pts1, pts2 = [np.loadtxt(f) for f in sys.argv[3:5]]

img1, img2 = [cv2.resize(i, None, fx=0.5, fy=0.5) for i in [img1, img2]]
pts1, pts2 = [p/2 for p in [pts1, pts2]]
h1, w1 = img1.shape[:2]
h2, w2 = img2.shape[:2]

c1, c2 = img1.copy(), img2.copy()
for i, pair in enumerate(zip(pts1, pts2)):
    p1, p2 = [tuple(p.astype(np.int32)) for p in pair]
    cv2.circle(c1, p1, 3, (0,0,255), -1)
    cv2.circle(c2, p2, 3, (0,0,255), -1)
    cv2.putText(c1, str(i+1), p1, cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255))
    cv2.putText(c2, str(i+1), p2, cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255))

cv2.imshow("Image 1", c1)
cv2.imshow("Image 2", c2)
cv2.moveWindow("Image 1", 0, 50)
cv2.moveWindow("Image 2", w1, 50)
cv2.waitKey()
cv2.destroyAllWindows()

"""
Compute F and stereo rectification.
"""
F, mask = cv2.findFundamentalMat(pts1, pts2)
ret, H1, H2 = cv2.stereoRectifyUncalibrated(pts1, pts2, F, (w1, h1))

#bounds1 = cv2.perspectiveTransform(np.array([[[0,0], [w1, 0], [0, h1], [w1, h1]]], np.float32), H1)[0]
#bounds2 = cv2.perspectiveTransform(np.array([[[0,0], [w2, 0], [0, h2], [w2, h2]]], np.float32), H2)[0]
#bounds = np.array([bounds1.ptp(axis=0), bounds2.ptp(axis=0)]).max(axis=0).astype(np.int32)

bounds = (w1, h1) # Use this instead of the above 3 lines for smaller manageable images.

"""
Warp input images.
"""
rect1 = cv2.warpPerspective(img1, H1, tuple(bounds))
rect2 = cv2.warpPerspective(img2, H2, tuple(bounds))
mask1 = np.all(rect1 != 0, axis=-1)
mask2 = np.all(rect2 != 0, axis=-1)
mask = (mask1*mask2)[..., np.newaxis]

# Absolute difference via optical flow
rect1g = cv2.cvtColor(rect1, cv2.COLOR_BGR2GRAY)
rect2g = cv2.cvtColor(rect2, cv2.COLOR_BGR2GRAY)
flow = cv2.calcOpticalFlowFarneback(rect1g, rect2g, None, 0.5, 3, 15, 3, 5, 1.2, 0)
diff = abs(rect1.astype(np.float32)*mask - warp_flow(rect2, -flow).astype(np.float32)*mask).mean(-1)

cv2.imshow("Rectified 1", rect1)
cv2.imshow("Rectified 2", rect2)
cv2.imshow("Difference (opt-flow warped)", diff / diff.max())
cv2.moveWindow("Rectified 1", 0, 50)
cv2.moveWindow("Rectified 2", rect1.shape[1], 50)
cv2.moveWindow("Difference (opt-flow warped)", 2*rect1.shape[1], 50)

# Block matching
from match import block_match
rect1g = cv2.cvtColor(rect1, cv2.COLOR_BGR2GRAY).astype(np.float32)
rect2g = cv2.cvtColor(rect2, cv2.COLOR_BGR2GRAY).astype(np.float32)
win_size=21; max_disp=32
mad = block_match(rect1g.astype(np.float32), rect2g.astype(np.float32),
                  win_size, max_disp)
min_mad = mad.min(-1)
max_mad = mad.max(-1)

cv2.imshow("Mean Abs. Diff. (min over blocks)", (min_mad/min_mad.max())*mask[...,0])
cv2.moveWindow("Mean Abs. Diff. (min over blocks)", 0, rect1.shape[0])
cv2.waitKey()
