#pragma once

#include <Python.h>
#include <numpy/arrayobject.h>

template<typename T>
class npyArray {
    public:
        npyArray(PyArrayObject* p): arr(p) {}

        const npy_intp size(const int& ind) const {
            return PyArray_DIMS(arr)[ind];
        }

        const npy_intp ndim() const {
            return PyArray_NDIM(arr);
        }

        const T& operator()(const int& i) const { return *_access_1(i); }
        T& operator()(const int& i) { return *_access_1(i); }

        const T& operator()(const int& i, const int& j) const { return *_access_2(i,j); }
        T& operator()(const int& i, const int& j) { return *_access_2(i,j); }

        const T& operator()(const int& i, const int& j, const int& k) const { return *_access_3(i,j,k); }
        T& operator()(const int& i, const int& j, const int& k) { return *_access_3(i,j,k); }

        const T& operator()(const int& i, const int& j, const int& k, const int& l) const { return *_access_4(i,j,k,l); }
        T& operator()(const int& i, const int& j, const int& k, const int& l) { return *_access_4(i,j,k,l); }

        inline T* _access_4(const int& i, const int& j, const int& k, const int& l) { return ((T*)PyArray_GETPTR4(arr, i, j, k, l)); }
        inline T* _access_3(const int& i, const int& j, const int& k) { return ((T*)PyArray_GETPTR3(arr, i, j, k)); }
        inline T* _access_2(const int& i, const int& j) { return ((T*)PyArray_GETPTR2(arr, i, j)); }
        inline T* _access_1(const int& i) { return ((T*)PyArray_GETPTR1(arr, i)); }

        inline const T* _access_4(const int& i, const int& j, const int& k, const int& l) const { return ((const T*)PyArray_GETPTR4(arr, i, j, k, l)); }
        inline const T* _access_3(const int& i, const int& j, const int& k) const { return ((const T*)PyArray_GETPTR3(arr, i, j, k)); }
        inline const T* _access_2(const int& i, const int& j) const { return ((const T*)PyArray_GETPTR2(arr, i, j)); }
        inline const T* _access_1(const int& i) const { return ((const T*)PyArray_GETPTR1(arr, i)); }

    private:
        PyArrayObject* arr;
};
