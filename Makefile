.PHONY: all clean
all: setup.py match.cpp
	python setup.py build_ext --inplace

clean:
	rm *.so
