from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy as np

setup(
        ext_modules = [
            Extension(
                "match",
                sources = ["match.cpp"],
                include_dirs = [np.get_include()],
                extra_compile_args = ["-msse", "-msse2", "-msse3", "-mfpmath=sse", "-O3", "-fopenmp"],
                extra_link_args = ["-O3", "-fopenmp"],
                ),
            ],
        cmdclass={"build_ext" : build_ext}
        )
